# Containerization with Docker

## Content Learning Objectives

After completing this activity, students should be able to:

*

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1 - Overview

![Model 1](model.jpg)

1. Model 1 depicts four methods of deployment of applications. Write a title and a description for each method.

    Method | Title | Description
    ------- | ----- | -----------
    A | |
    B | |
    C | |
    D | |

2. For each characteristic below, place each method on a scale of *least* to *most*. Be prepared to share your reasoning.

    1. Efficiently uses hardware and operating system resources.

        ```
        least                        most

        ```

    2. Is difficult to install/deploy due to incompatabilities between requirements of applications and their dependencies.

        ```
        least                        most

        ```

    3. Is fast to deploy an application and all its dependendencies. Include the operating system and hardware as a dependency if it is not shared with other applications.

        ```
        least                        most

        ```

    4. Requires applications and their dependencies to built for the same operating system?

        ```
        least                        most

        ```

    5. Demands a lot of hardware resources from each machine.

        ```
        least                        most

        ```

    6. Allows applications and their dependencies to interfere with each other.

        ```
        least                        most

        ```

## Dockerfiles, Images, and Containers

```
Dockerfile -- build --> Image -- run --> Container_1
                              \-- run --> Container_2
                               \-- run --> Container_3
```

## 

```
  Environment Variables
  (parameters)
     |
     v
-------------
| Container |<------> Ports (networking)
-------------
     ^
     |
     v
  Volumes
  (data/configuration and persistence)
```

1. Why might you use an enviornment variable when you run a container? (hint: what if you are running different containers based on the same image?)

2. 

## Install Docker

Do this at home and answer the following after.

1. Did you get it installed and does it work?

2. Did you have any problems? If so,

    1. What went wrong?
    2. What did you do to try to fix it?
    3. Where you ultimately successful?

3. What questions did you have while you did this?


## Read and Complete "Getting Started"

Do this at home. Be sure to log any questions you have.

https://www.docker.com/101-tutorial

1. What questions do you have?

2. What's one thing you found interesting or important?



---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
