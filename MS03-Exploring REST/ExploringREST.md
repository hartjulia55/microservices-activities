# Exploring REST API Calls

## Content Learning Objectives

After completing this activity, students should be able to:

* Read the OpenAPI specification for a REST API.
* Modify an existing OpenAPI specification to change, extend, or add a path.

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

For this activity, you should have a *Technician* role. The Technician will be the one to have Visual Studio Code running, and the repositories open in Dev Containers. You may want to make sure your Technician is a team member with a more powerful computer.

You can combine the Technician role with another role if you do not have enough team members.

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Technician |
Presenter |
Recorder |
Reflector |

## Prerequisites

You should have a [MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit) GitLab subgroup that has been created for you (either by your instructor, your team leader, or yourself). It will contain a number of respositories. (These activities are likely in a Microservices Activities repository in this subgroup.)

If not, you should follow the instructions in the [README for the MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microserviceskit/-/blob/main/README.md) to create one.

You need to have, at least, the GuestInfoAPI and GuestInfoBackend repositories cloned to your computer to work on this activity.

## Model 1: Exploring the GuestInfoSystem API Using Swagger Preview

### Open the GuestInfoAPI Repository in Visual Studio Code

1. Start Visual Studio Code.
2. Open the folder for the GuestInfoAPI.
3. When Visual Studio Code asks, click `Reopen in Container`.
4. Open the README file in the GuestInfoAPI repository inside the MicroservicesKit, and read the file.
    * You are reading to get an overview of the project and what tools and documentation are available.
    * You do not need to follow the links in the document, but take note of what is there.
5. Open the GuestInfoSystem docs file at `docs/openapi.yaml`.
6. Open the `Command Palette` (`F1` or right-click or `Ctrl+Shift+P`).
   1. Open with `Preview Swagger`.
   2. Arrange the `openapi.yaml` file and the `Swagger Preview` windows to be side by side, so that you can see both as you answer the questions.

### Questions - Model 1 (? min)

1. What is the version number for `GuestInfoSystemAPI`?
   1. Where does that appear in the `index.yaml` file?
   2. Where does that appear in the preview?
2. What version of the OpenAPI specification is being used?
   1. Where does that appear in the `index.yaml` file?
   2. Where does that appear in the preview?
3. How many REST API endpoints (paths) are there for `GuestInfoSystemAPI`? List them.
   1. Where did you find them in the preview?
   2. Where did you find them in the `index.yaml` file? What is the section called?

   > ***STOP - Do not continue until asked to do so by the instructor.***

4. How many different HTTP methods are used? List them.
5. Based on the notes in the table in `MS02 Introduction to REST API Calls, Model 2`, what is each of the HTTP methods in `GuestInfoSystemAPI` used for?
   1. `GET /guests`
   2. `POST /guests`
   3. `GET /guests/{wsuID}`
   4. `PUT /guests/{wsuID}`
   5. `DELETE /guests/{wsuID}`
   6. `GET /version`
6. What is the difference between the two rows in that table in `MS01 - Model 2`?
   1. Which paths in `GuestInfoSystemAPI` use the HTTP methods as given on the first row?
   2. Which paths use in `GuestInfoSystemAPI` the HTTP methods as given on the second row?

   > ***STOP - Do not continue until asked to do so by the instructor.***

7. Look at the `GuestInfoSystemAPI` endpoints **in the preview**.
   1. Expand the `GET /guests` endpoint by clicking on it.
      1. Look at the `Parameters` section.
         1. What is the name of the optional parameter?
         2. What is the type of the optional parameter?
         3. This is a `query` parameter.
            1. Where is that represented in the preview?
            2. Write an example of how you would use it in an API call. (Look at MS02 - Model 2 if you don't remember the form.)
      2. Look at the `Responses` section.
         1. How many possible responses are there from this endpoint?
         2. List them in the table below - code, description, and an explanation of why you think you might get this response. (You may want to look at the [list of HTTP response codes](https://www.restapitutorial.com/httpstatuscodes.html).)

            Code | Description | Why might you get this response?
            --- | --- | ---
            200 |     |

         3. The response bodies are in JSON format.
            1. Look at the [JSON Format](https://www.json.org/json-en.html) through `object` and `array`.
            2. Explain the `Example Value` for the first response.. What does it contain and how it is formatted?
            3. Look at the `Schema`. Did you learn something else about the response format that you missed when looking at the example value?
         4. Compare the response body to what you saw in MS02 - Model 1.
   2. `POST /guests`
      1. How many possible responses are there from this endpoint?
      2. List them in the table below - code, description, and an explanation of why you think you might get this response. (You may want to look at the [list of HTTP response codes](https://www.restapitutorial.com/httpstatuscodes.html).)

         Code | Description | Why might you get this response?
         --- | --- | ---

      3. What is missing that was in `GET /guests`?
      4. What new section appears that was not in the `GET /guests` endpoint?
      5. Explain the value in this section. What is it used for?
      6. Why does this end point have both a Request Body and a Response Body?
   3. `GET /guests/{wsuID}`
      1. Explain the parameter section. What is different about the parameter in `GET /guests`?
      2. How many possible responses are there from this endpoint?
      3. List them in the table below - code, description, and an explanation of why you think you might get this response. (You may want to look at the [list of HTTP response codes](https://www.restapitutorial.com/httpstatuscodes.html).)

         Code | Description | Why might you get this response?
         --- | --- | ---

      4. How is the `200` Response Body different from the one in `GET /guests`? Why?
      5. Why does this section have a `404` response?
      6. Why was there no `404` response in `GET /guests`?
   4. `PUT /guests/{wsuID}`
      1. How many possible responses are there from this endpoint?
      2. List them in the table below - code, description, and an explanation of why you think you might get this response. (You may want to look at the [list of HTTP response codes](https://www.restapitutorial.com/httpstatuscodes.html).)

         Code | Description | Why might you get this response?
         --- | --- | ---

      3. What is the difference in response codes between `PUT` and `POST`?  Explain the difference in the response codes.
   5. `DELETE /guests/{wsuID}`
      1. How many possible responses are there from this endpoint?
      2. List them in the table below - code, description, and an explanation of why you think you might get this response. (You may want to look at the [list of HTTP response codes](https://www.restapitutorial.com/httpstatuscodes.html).)

         Code | Description | Why might you get this response?
         --- | --- | ---

   6. `GET /version`
      1. How many possible responses are there from this endpoint?
      2. List them in the table below - code, description, and an explanation of why you think you might get this response. (You may want to look at the [list of HTTP response codes](https://www.restapitutorial.com/httpstatuscodes.html).)

         Code | Description | Why might you get this response?
         --- | --- | ---

      3. Why is this endpoint's name singluar, instead of plural?

   > ***STOP - Do not continue until asked to do so by the instructor.***

## Model 2: Using the GuestInfoSystem REST API with Visual Studio Code

### Open the GuestInfoBackend Repository in Visual Studio Code

1. Start Visual Studio Code.
2. Open the folder for the GuestInfoBackend.
3. When Visual Studio Code asks, click `Reopen in Container`.
4. Open a `Terminal`
   1. `Terminal` menu -> `New Terminal`
5. Start the GuestInfoBackend (which includes backend, database, and message queue) with the command

```bash
commands/rebuild.sh
```

* This command will shutdown any running version of the system, rebuild the backend server code, and start it up.
* The command will continue running in this terminal window, and will display logging input from the system.
* You can stop the system by typing `Crtl + C` in the terminal window.
* Using the rebuild command will rebuild the docker image and remove all data from the database.
* If you have not changed any of the backend source code, you can use the following command to shutdown and restart the system (deleting all the data in the database). This will be faster than rebuilding.

```bash
commands/restart.sh
```

### View the API Documentation

You will want to have the API documentation open for reference.

1. Open the file at `lib/openapi.yaml`.
2. Open the `Command Palette` (`F1` or right-click or `Ctrl+Shift+P`).
   1. Open with `Preview Swagger`.

### Questions - Model 2 (? min)

1. Find the port for the backend-server.
   1. Open a new terminal window.
   2. Run the command `docker ps`.
   3. Record the port for the **backend** here:
2. Open the file `testing/manual/calls.http`.
This file contains REST API calls which can be made using the `rest-client` Visual Studio Code extension to run them. (Any name ending in `.http` will work for this extension.)
3. Try out the `GET http://localhost:10350/version` endpoint.
   1. Click on `Send Request` above the call. A new window should open for the response.
   2. Explain the parts of the request. You may want to refer to the API documentation.
   3. Record the response.
   4. Explain the first line and the last line of the response. You may want to refer to the API documentation.
4. Notice the `###` separator between requests.
   1. Try removing it.
   2. What happened to the requests it is between?
   3. Put it back.
5. Try out the `GET http://localhost:10350/guests` endpoint.
   1. Explain the parts of the request. You may want to refer to the API documentation.
   2. Record the response below:
   3. Explain the first line and the last line of the response. You may want to refer to the API documentation.
6. Let's add a guest.
   1. Scroll down to the first of the `POST` requests. (The one with the `wsuID` of `1234567`.)
   2. What is different about that line (other than `POST`)?
   3. What is on the following line?
   4. What is on the following line?
   5. What are the remaining lines (until the `###` separator)? You may want to look at the API documentation again.
   6. Click `Send Request`
   7. Explain the first line of the response.
   8. Explain line 4 of the response.
   9. Explain lines 11-29 of the response.
7. Make the `GET /guests` request again to see how it has changed. How is it different?
8. Add a few more guests (at least 2 more). You can use the second `POST` to create one. Create your own `POST`s for the remaining guests. Get the list again. How is it different?
9. Now, let's try filtering the list of guests by whether or not they are a resident. Make the `GET http://localhost:10350/guests?resident=true` call to get all the residents. Modify it and try it again to get the non-residents.
10. Now, let's try getting a single guest from the server. Make the `GET http://localhost:10350/guests/1234567` request. Explain the response.
11. Let's modify the information about a guest. Find the `PUT` call in the `calls.http` file.
    1. What will this call change?
    2. Make the `PUT` call.
    3. Explain the response.
12. Now, let's try deleting an item. Call the `DELETE http://localhost:10350/guests/1234567` request. Explain the response.

## Model 3: Designing Tests for the GuestInfoBackend

Testing your backend for correct behaivor is very important. You would normally want automated tests that you could run every time you modify the system to make sure that it all still works. We will run some tests manually, but we can later translate these into automated tests.

But first, we need to design the tests. We want to, at the very least, test every possible response from the endpoints.

1. Setup - You will want to have some number of calls to prepare the data in the database for your test(s). For example, you need to have a known guest in the database to be able to test a successful deletion.
2. Test - You will want one or more calls to make sure that system actually does what you want it to.
3. Tear down - You may need to return the database to a know state before moving on to the next test.

You will want to both successful and unsuccessful tests. For example, in addition to the successful deletion test, you should also make sure the system correctly handles an unsuccessful deletion - trying to delete a guest that is not in the database.

### Questions - Model 3 (? min)

1. Design tests for each of the possible responses for `GET /guests`. For each specify the Setup calls, Test(s) call(s), and what you expect for a response (code and returned data.) You cannot test the 500 responses this way.

   You do not need to write full, syntactically correct API calls - just enough to know what is needed.
   * 200
      * Setup
      * Tests
      * Expected response
   * 400
      * Setup
      * Tests
      * Expected response
2. Design tests for each of the possible responses for `GET /guests/wsuID`. For each specify the Setup calls, Test(s) call(s), and what you expect for a response (code and returned data.)

   You do not need to write full, syntactically correct API calls - just enough to know what is needed.
   * 200
      * Setup
      * Tests
      * Expected response
   * 400
      * Setup
      * Tests
      * Expected response
   * 404
      * Setup
      * Tests
      * Expected response

## Model 4 - Testing the GuestInfoBackend

### Questions - Model 4 (? min)

1. Translate the your tests for `GET /guests` into actual API calls. Create a new `testing/manual/4.1-calls.http` directory and add your tests. Run the tests to verify that the backend works correctly.
1. Translate the your tests for `GET /guests/wsuId` into actual API calls. Create a new `testing/manual/4.2-calls.http` directory and add your tests. Run the tests to verify that the backend works correctly.

## Stop the GuestInfoBackend

When finished, stop the system with the command:

   ```bash
   commands/down.sh
   ```

---

&copy; 2022 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
