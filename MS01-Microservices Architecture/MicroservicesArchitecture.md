# Software Architectures from Monolith to Microservices

Software Architecture concerns the top-level structure of a software system - its top-level components, what services they provide, and how they interact with each other.

## Content Learning Objectives

After completing this activity, students should be able to:

* Identify the differences between Monolith, Client-Server, and Microservices Architectures.
* Describe advantages and disadvantages to each of the architectures.

## Process Skill Goals

During the activity, students should make progress toward:

* Information Processing - interpret architecture diagrams

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Technician/Reflector |

## Model 1 - Monolith Architecture

```plantuml
@startuml
actor Admin
actor Staff

node "Thea's Pantry System" <<server>>  {
  [\nCheckInGuest\nCheckOutGuest\nAddInventory\nCheckInventory\nGenerateWCFBReport] <<application>> as TPSApp
  Database Database {
    [\nGuestInfo\nInventory\nReporting] <<tables>> as DB
  }
}

Staff -> TPSApp
Admin -> TPSApp
TPSApp <--> DB
@enduml
```

### Questions - 15 min (Model 1)

1. How many separate software applications does the monolith version have?
2. How many different functions does the application perform? Name them.
3. How many databases does the monolith version have?
4. How many different tables does the database have? Name them.
5. How many servers does the monolith version use?
6. If the database became corrupted or unavailable, what functionality would be impacted?
7. Suppose this system is being maintained by multiple development teams (one for each function) and one operations team (responsible for ensuring the integrity &mdash; security, stability, availability, etc. &mdash; of the system in production). You are on the Inventory team. You want to update the schema for the database to improve the functionality of the Inventory functions. (This will also require updating the application.) How easy do you think it will be to update the database and get the new database into production and why?
8. Would updating the user interface require re-deploying the entire system?

## Model 2 - Client-Server Architecture

```plantuml
@startuml
actor Admin
actor Staff

node AdminComputer {
  [\nCheckInventory\nGenerateWCFBReport] <<application>> as AdminApp
}
node StaffComputer {
  [\nCheckInGuest\nCheckOutGuest\nAddInventory] <<application>> as StaffApp
}
node "Thea's Pantry Backend" {
  [\nCheckInGuest()\nCheckOutGuest()\nAddInventory()\nCheckInventory()\nGenerateWCFBReportFrontend()] <<server>> as TPBE
  Database Database {
    [\nGuestInfo\nInventory\nReporting] <<tables>> as DB
  }
}

Staff -> StaffApp
Admin -> AdminApp
AdminApp --> TPBE
StaffApp --> TPBE
TPBE <--> DB
@enduml
```

### Questions - 15 min (Model 2)

1. How many separate software applications does the client-server version have?
2. How many different functions does the server perform? Name them.
3. How many different functions does the staff application perform? Name them.
4. How many different functions does the admin application perform? Name them.
5. How many different tables does the database have? Name them.
6. How many computers/servers does the client-server version use?
7. If the database became corrupted or unavailable, what functionality would be impacted?
8. Suppose this system is being maintained by multiple development teams (one for each function) and one operations team (responsible for ensuring the integrity &mdash; security, stability, availability, etc. &mdash; of the system in production). You are on the Inventory team. You want to update the schema for the database to improve the functionality of the Inventory functions. (This will also require updating the application.) How easy do you think it will be to update the database and get the new database into production and why?
9. Would updating the user interface require re-deploying the entire system?
10. Would it be possible to have both a PC app and a mobile app?
11. Would it be possible to divide up responsibilities by having a team for each app and one for the server? Would this be easier for the teams to work separately? What would they will have to coordinate on?

## Model 3 - Microservices Architecture

Notes:

1. The arrows represent calls from one system to another, not the dataflow direction.
2. NGINX is a web server.
3. Express.js is a backend server framework.

```plantuml
@startuml

skinparam ranksep 150

Actor Staff
Actor Admin

node "StaffBrowser" {
  [CheckInGuestFrontend] as BrowserCheckIn
  [CheckOutGuestFrontend] as BrowserCheckOut
  [AddInventoryFrontend] as BrowserAddInventory
}

node "AdminBrowser" {
  [CheckInventoryFrontend] as BrowserCheckInventory
  [GenerateWCFBReportFrontend] as BrowserGenerateReport
}

package "GuestInfoSystem" {
  node NGINX <<Server>> as GuestInfoNGINX {
    [CheckInGuestFrontend]
  }

  node MongoDB <<Server>> as GuestInfoMongoDB{
    database GuestInfoDb
  }

  node Express.js <<Server>> as GuestInfoExpress.js{
    [GuestInfoBackend]
  }
  GuestInfoBackend --> GuestInfoDb : guestinfo-backend-db-network
}

package "InventorySystem" {
  node NGINX <<Server>> as AddInventoryNGINX {
    [AddInventoryFrontend]
  }

  node NGINX <<Server>> as CheckOutGuestNGINX {
    [CheckOutGuestFrontend]
  }
  
  node NGINX <<Server>> as CheckInventoryNGINX {
    [CheckInventoryFrontend]
  }

  node MongoDB <<Server>> as InventoryMongoDB {
    database InventoryDb
  }

  node Express.js <<Server>> as InventoryExpress.js{
    [InventoryBackend]
  }
  InventoryBackend --> InventoryDb : inventory-backend-db-network
}

package "ReportingSystem" {
  node NGINX <<Server>> as ReportingNGINX {
    [GenerateWCFBReportFrontend]
  }

  node MongoDB <<Server>> as ReportingMongoDB {
    database ReportingDb
  }

  node Express.js <<Server>> as ReportingExpress.js {
    [ReportingBackend]
  }
  ReportingBackend --> ReportingDb : reporting-backend-db-network
}

package "EventSystem" {
  node RabbitMQ <<Server>> {
    queue InventoryEvents
    queue GuestInfoEvents
  }
}

Staff --> StaffBrowser
Admin --> AdminBrowser

CheckInGuestFrontend ..> BrowserCheckIn
CheckOutGuestFrontend .up.> BrowserCheckOut
AddInventoryFrontend .up.> BrowserAddInventory
CheckInventoryFrontend .up.> BrowserCheckInventory
GenerateWCFBReportFrontend .up.> BrowserGenerateReport
BrowserCheckIn --> GuestInfoBackend
BrowserCheckOut --> InventoryBackend
BrowserAddInventory --> InventoryBackend
BrowserCheckInventory --> InventoryBackend
BrowserGenerateReport --> ReportingBackend
ReportingBackend ----> InventoryEvents : events-network
ReportingBackend ----> GuestInfoEvents : events-network
InventoryBackend ----> InventoryEvents : events-network
GuestInfoBackend ----> GuestInfoEvents : events-network

@enduml
```

### Questions - 20 min (Model 3)

1. How many separate software applications does the microservices version have? (Include servers and browsers.)
2. How many servers does the microservices version use?
3. Which server does each of the functions run on?
    * CheckInventory
    * AddInventory
    * GenerateWCFBReport
    * CheckInGuest
    * CheckOutGuest
4. How many databases does the microservices version use?
5. Which database are each of the tables on?
    * GuestInfo
    * Inventory
    * Reporting
6. If a database became corrupted or unavailable, what functionality would be impacted?
7. Suppose this system is being maintained by multiple development teams (one for each function) and one operations team (responsible for ensuring the integrity &mdash; security, stability, availability, etc. &mdash; of the system in production). You are on the Inventory team. You want to update the schema for the database to improve the functionality of the Inventory functions. (This will also require updating the application.) How easy do you think it will be to update the database and get the new database into production and why?
8. Would updating the inventory system require re-deploying the entire system?
9. Would it be possible to divide up responsibilities by having a team for each system? Would this be easier for the teams to work separately? What would they will have to coordinate on?
10. Under which of the architectures (monolith, client-server, microservices) would it be easiest to update components and why?
11. Under which of the architectures would it be safer to update components and why?
12. Under which of the architectures would the operations team prefer to delay updates for as long as possible and why?
13. Under which of the architectures would it be possible for different teams to develop in different languages or use different databases?
14. What do you think is the purpose of the EventSystem?
15. Which architecture is simplest to understand?

---

&copy; 2022 Karl R. Wurst <karl@w-sts.com> and Stoney Jackson <dr.stoney@gmail.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
